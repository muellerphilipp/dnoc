# DNoC

> A dead simple Dom NOde Creator returning HTMLElements.

DNoC is basically a wrapper around some useful methods of the [HTML DOM API](https://developer.mozilla.org/en-US/docs/Web/API/HTML_DOM_API). It allows you to create an HTMLElement using a straightforward function call.

## Install

```
npm install --save-dev dnoc
```

## Usage

Use DNoC to create an HTMLElement like so:

```JS
import { dom } from "dnoc";

const br = dom("br") // br.outerHTML == "<br>"
```

### Child nodes

As child nodes, you can add either simple TEXT_NODEs or other ELEMENT_NODEs:

```JS
import { dom } from "dnoc";

const p0 = dom("p", "Lorem ipsum")
  // p0.outerHTML == "<p>Lorem ipsum</p>"

const p1 = dom("p", ["Lorem ipsum", "Lorem ipsum"])
  // p1.outerHTML == "<p>Lorem ipsumLorem ipsum</p>"

const p2 = dom("p", ["Lorem ipsum", dom("em", "Dolor"), "Lorem ipsum"])
  // p2.outerHTML == "<p>Lorem ipsum<em>Dolor</em>Lorem ipsum</p>"
```

### Attributes

Set attributes for the HTMLElement you create:

```JS
import { dom } from "dnoc";

const input = dom("input", null, { type: "text" })
  // input.outerHTML == '<input type="text" />'

const div = dom("div", "Lorem ipsum", { class: "info", style: "color: #00f" })
  // div.outerHTML == '<div class="info" style="color: #00f">Lorem ipsum</div>'
```

### Event listeners

You can set event listeners for the HTMLElement you create:

```JS
import { dom } from "dnoc";

const button = dom("button", null, null, {
    "click": function () { console.log("click!") }
  })
  // <button> with a single listener logging “click!” when the button is clicked.

const button = dom("button", null, null, {
    "click": [
      function () { console.log("click!") },
      function () { console.log("clack!") }
    ]
  })
  // <button> with two listeners logging “click!” and “clack!” when the button is clicked.

const button = dom("button", null, null, {
    "click": [
      function () { console.log("click!") },
      { "once": true }
    ]
  })
  // <button> with a single listener logging “click!” only the first time the button is clicked.

const button = dom("button", null, null, {
    "click": [
      [
        function () { console.log("click!") },
        { "once": true }
      ],
      function () { console.log("clack!") }
    ]
  })
  // <button> with two listeners: one logging “click!” only the first time the button is clicked, the second logging “clack!” every time the button is clicked.
```

### Other usage options

You might also want to use DNoC functions to add content, attributes, or event listeners to existing nodes:

```JS
import { setAttributes, setContent, setEventListeners } from "dnoc";

setAttributes(document.body, { class: "bg" })
  // Adds the class "bg" to the <body> node.

setContent(document.body, "Lorem ipsum")
  // Appends a TEXT_NODE with "Lorem ipsum" to the <body> node.

setEventListeners(document.body, { click: () => { console.log("click!") }})
  // Sets an event listener for the <body> node.
```
