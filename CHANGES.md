# Changes

## v1.1.3 (2022-11-23)

### Fixes

- Minor refactoring of internal addEventListener function.

### Updates

- @types/jsdom v16.2.14 => v20.0.1
- @typescript-eslint/eslint-plugin v5.21.0 => v5.44.0
- @typescript-eslint/parser v5.21.0 => v5.44.0
- ava v4.2.0 => v5.1.0
- c8 v7.11.2 => v7.12.0
- eslint v8.14.0 => v8.28.0
- eslint-plugin-sonarjs v0.13.0 => v0.16.0
- eslint-plugin-tsdoc v0.2.16 => v0.2.17
- jsdom v19.0.0 => v20.0.3
- prettier v2.6.2 => v2.8.0
- typescript v4.6.3 => v4.9.3

## v1.1.2 (2022-04-26)

### Fixes

- Updated tsconfig.json and package.json for better bundler support.

### Updates

- @typescript-eslint/eslint-plugin v5.20.0 => v5.21.0
- @typescript-eslint/parser v5.20.0 => v5.21.0

## v1.1.1 (2022-04-24)

### Fixes

- README improved.

## v1.1.0 (2022-04-24)

### Features

- Add content to an exisiting node.
- Add/update attributes of an exisiting node.
- Add event listeners to an exisiting node.

### Fixes

- Removed return in .forEach call.
- Added eslint-plugin-tsdoc
- Added comments.

### Updates

- eslint v8.13.0 => v8.14.0

## v1.0.0 (2022-04-21)

### Features

- Create DOM node with via tag name.
- Provide child nodes and/or text content when creating a DOM node.
- Add attributes when creating a DOM node.
- Add event listeners when creating a DOM node.
