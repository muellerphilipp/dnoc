// eslint-disable-next-line no-undef
module.exports = {
  typescript: {
    rewritePaths: {
      "src/": "build/",
    },
    compile: false,
  },
  exclude: "./**/*.spec.js",
};
