// EXPORTS
export { dom, setAttributes, setContent, setEventListeners };
export default dom;

// TYPES
type AttrSet = { [key: string]: string };
type Content = string | HTMLElement;
type ContentSet = Content | Content[];
type ListenerParams<T extends keyof HTMLElementTagNameMap> = Parameters<
  HTMLElementTagNameMap[T]["addEventListener"]
>;
type ListenerDef<T extends keyof HTMLElementTagNameMap> =
  | ListenerParams<T>[1]
  | [ListenerParams<T>[1], ListenerParams<T>[2]];
type ListenerSet<T extends keyof HTMLElementTagNameMap> = {
  [K in keyof HTMLElementEventMap]?: ListenerDef<T> | ListenerDef<T>[];
};

// TYPE GUARDS
/**
 * Tests if the provided thing is a ListenerDef.
 *
 * @param specimen - The thing to be tested.
 */
function isListenerDef<T extends keyof HTMLElementTagNameMap>(
  specimen: unknown
): specimen is ListenerDef<T> {
  return (
    Array.isArray(specimen) &&
    specimen.length == 2 &&
    typeof specimen[0] == "function" &&
    typeof specimen[1] != "function"
  );
}

// FUNCS
/**
 * Adds content to an existing doucment fragment.
 *
 * @param fragment - Document fragment to add content to.
 * @param contentElement - Content to be added to the fragment.
 */
function setFragmentContent(
  fragment: DocumentFragment,
  contentElement: Content
): void {
  if (typeof contentElement === "string")
    fragment.appendChild(document.createTextNode(contentElement));
  else fragment.appendChild(contentElement);
}

/**
 * Adds content to an existing node.
 *
 * @param node - The node to set the event listeners for.
 * @param content - The content to be added.
 */
function setContent(node: HTMLElement, content: ContentSet): void {
  const fragment = document.createDocumentFragment();

  if (!Array.isArray(content)) content = [content];

  content.forEach((element): void => {
    setFragmentContent(fragment, element);
  });

  node.appendChild(fragment);
}

/**
 * Adds or updates attributes of an existing node.
 *
 * @param node - The node to set the attributes for.
 * @param attribtues - The new or updated attributes.
 */
function setAttributes(node: HTMLElement, attribtues: AttrSet): void {
  for (const property in attribtues) {
    node.setAttribute(property, attribtues[property]);
  }
}

/**
 * Adds event listeners to an existing node.
 *
 * @param node - The node to set the event listeners for.
 * @param listenerSet - A set of new event listeners.
 */
function setEventListeners<K extends keyof HTMLElementTagNameMap>(
  node: HTMLElementTagNameMap[K],
  listenerSet: ListenerSet<K>
): void {
  for (const type in listenerSet) {
    let listeners = listenerSet[type as keyof HTMLElementEventMap];

    if (listeners) {
      if (!Array.isArray(listeners) || isListenerDef(listeners))
        listeners = [listeners];

      listeners.forEach(
        addEventListenerCallback(node, type as keyof HTMLElementEventMap)
      );
    }
  }
}

/**
 * Creates the forEach callback function to add event listeners to the given node.
 *
 * @param node - The node to set the event listeners for.
 * @param type - A set of new event listeners.
 */
function addEventListenerCallback<K extends keyof HTMLElementTagNameMap>(
  node: HTMLElementTagNameMap[K],
  type: keyof HTMLElementEventMap
) {
  return (listenerDef: ListenerDef<K>): void => {
    if (!Array.isArray(listenerDef)) node.addEventListener(type, listenerDef);
    else node.addEventListener(type, ...listenerDef);
  };
}

/**
 * Returns an HTMLElement.
 *
 * @param tag - The tag name of the HTMLElement to be created.
 * @param content - The content of the HTMLElement to be created.
 * @param attribtues - The attribtues for the HTMLElement to be created.
 * @param listeners - The event listeners for the HTMLElement to be created.
 */
function dom<K extends keyof HTMLElementTagNameMap>(
  tag: K,
  content?: ContentSet | null,
  attribtues?: AttrSet | null,
  listeners?: ListenerSet<K>
): HTMLElementTagNameMap[K] {
  const node = document.createElement(tag);

  if (content) setContent(node, content);
  if (attribtues) setAttributes(node, attribtues);
  if (listeners) setEventListeners(node, listeners);

  return node;
}
