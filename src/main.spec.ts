import test from "ava";
import { JSDOM } from "jsdom";
import { dom } from "./main.js";

global.document = new JSDOM(`<html><body></body></html>`).window.document;

test("node without content (<br>)", (t) => {
  const br = dom("br");
  t.is(br.outerHTML, "<br>");
});

test("node with textContent (<span>)", (t) => {
  const span = dom(
    "span",
    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua."
  );
  t.is(
    span.outerHTML,
    "<span>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</span>"
  );
});

test("node with only attributes (<input>)", (t) => {
  const input = dom("input", null, { type: "text" });
  t.is(input.outerHTML, '<input type="text">');
});

test("node with TEXT_NODE and ELEMENT_NODE (p)", (t) => {
  const p = dom("p", [
    "orem ipsum dolor sit amet, consetetur sadipscing elitr,",
    dom("em", "sed diam nonumy eirmod tempor invidunt ut labore"),
    " et dolore magna aliquyam erat, sed diam voluptua.",
  ]);
  t.is(
    p.outerHTML,
    "<p>orem ipsum dolor sit amet, consetetur sadipscing elitr,<em>sed diam nonumy eirmod tempor invidunt ut labore</em> et dolore magna aliquyam erat, sed diam voluptua.</p>"
  );
});

test("node with only one listener (<button>)", (t) => {
  let check = 0;
  const button = dom("button", null, null, {
    click: function (this: HTMLButtonElement) {
      check = Math.random();
      this.dataset.check = check.toString();
    },
  });
  button.click();
  t.is(button.outerHTML, `<button data-check="${check}"></button>`);
});

test("node with only one listener and options (<button>)", (t) => {
  const check: [number, number] = [0, 1];
  let iteration = 0;
  const button = dom("button", null, null, {
    click: [
      function (this: HTMLButtonElement) {
        check[iteration] = Math.random();
        this.dataset["check" + iteration] = check[iteration].toString();
        iteration++;
      },
      { once: true },
    ],
  });
  button.click();
  button.click();
  t.is(button.outerHTML, `<button data-check0="${check[0]}"></button>`);
});

test("node with two listeners (<button>)", (t) => {
  const check: [number, number] = [0, 1];
  const button = dom("button", null, null, {
    click: [
      function (this: HTMLButtonElement) {
        check[0] = Math.random();
        this.dataset.check0 = check[0].toString();
      },
      function (this: HTMLButtonElement) {
        check[1] = Math.random();
        this.dataset.check1 = check[1].toString();
      },
    ],
  });
  button.click();
  t.is(
    button.outerHTML,
    `<button data-check0="${check[0]}" data-check1="${check[1]}"></button>`
  );
});

test("node with two listeners, and options (<button>)", (t) => {
  const check0: [number, number] = [0, 1],
    check1: [number, number] = [0, 1];
  let iteration0 = 0,
    iteration1 = 0;

  const button = dom("button", null, null, {
    click: [
      [
        function (this: HTMLButtonElement) {
          check0[iteration0] = Math.random();
          this.dataset["check0." + iteration0] = check0[iteration0].toString();
          iteration0++;
        },
        { once: true },
      ],
      function (this: HTMLButtonElement) {
        check1[iteration1] = Math.random();
        this.dataset["check1." + iteration1] = check1[iteration1].toString();
        iteration1++;
      },
    ],
  });
  button.click();
  button.click();
  t.is(
    button.outerHTML,
    `<button data-check0.0="${check0[0]}" data-check1.0="${check1[0]}" data-check1.1="${check1[1]}"></button>`
  );
});
